require 'sinatra/base'

module Simplerb
  class Base < Sinatra::Base
    HTTP_VERBS = ['get', 'post', 'put', 'patch', 'delete']

    HTTP_VERBS.each do |verb|
      send verb.to_sym, '/*' do
        path  = params[:splat].first
        views = view_files('[^_]*.erb')
        if views.include? File.join(settings.views, "#{path}.erb") 
          erb path.to_sym
        elsif views.include? File.join(settings.views, path, 'index.erb')
          erb File.join(path, 'index').to_sym
        else
          pass
        end
      end
    end

    helpers do
      def view_files(pattern = '*')
        Dir.glob(File.join(settings.views, '**', pattern))
      end

      def partial(partial)
        if view_files('_*.erb').include? File.join(settings.views, "_#{partial}.erb")
          erb "_#{partial}".to_sym, layout: false
        end
      end

      HTTP_VERBS.each do |verb|
        define_method "http_#{verb}?" do
          request.request_method == verb.upcase
        end
      end
    end
  end
end
